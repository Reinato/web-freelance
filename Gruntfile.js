module.exports = function (grunt) {

    require('time-grunt')(grunt);

    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',         //carpeta origen
                    src: ['*.scss'],    //archivos a compilar
                    dest: 'css',        //carpeta destino
                    ext: '.css'         //archivo destino
                }]
            }
        },

        watch: {
            files: ['css/*.scss'],      // ubicacion y archivos a escuchar
            tasks: ['css']              // tarea a ejecutar
        },

        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'images/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    //for font
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },

        clean: {
            build: {
                src: ['dist/']
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                //filerev: release hashes(md5) all assets(images,js and css)
                // in dist directory
                files: [{
                    src: ['dist/js/*.js', 'dist/css/*.css']
                }]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'precios.html', 'contacto.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }

    });


    grunt.registerTask('css', ['sass']);                        //tareas css
    grunt.registerTask('default', ['browserSync', 'watch']);    //tareas browser-sync
    grunt.registerTask('img:compress', ['imagemin']);            //tareas css
    grunt.registerTask('build', [
        'clean',                //Borramos el contenido de dist
        'copy',                 //Copiamos los archivos html a dist
        'imagemin',             //Optimizamos imagenes y las copiamos a dist
        'useminPrepare',        //Preparamos la configuracion de usemin
        'concat',
        'cssmin',
        'uglify',
        'filerev',              //Agregamos cadena aleatoria
        'usemin'                //Reemplazamos las referencias por los archivos generados por filerev
    ]);
};