$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 5000
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log('el modal se está mostrando');

        $('#btnContacto').removeClass('btn btn-outline-success');
        $('#btnContacto').addClass('btn btn-secondary');
        $('#btnContacto').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('el modal se mostro');
    });

    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('el modalse está ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('el modal ya se oculto');
        $('#btnContacto').removeClass('btn btn-secondary');
        $('#btnContacto').addClass('btn btn-outline-success');
        $('#btnContacto').prop('disabled', false);
    });
});
